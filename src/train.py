import os
import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow import keras as K
import modelsCNN
import argparse
from sklearn.metrics import balanced_accuracy_score, confusion_matrix, roc_curve
from sklearn.utils import class_weight

parser = argparse.ArgumentParser()
parser.add_argument('--model', '-m', type=int, default=1)
parser.add_argument('--epochs', '-e', type=int, default=100)
parser.add_argument('--batch', '-b', type=int, default=64)
args = parser.parse_args()

batch_size = args.batch
img_size = 3
channels = 64
epochs = args.epochs
model_name = 'model' + str(args.model) + '_CNN'


dataset_dir = '/users/ppalmier/df_data/'
model_dir = './models/' + model_name + '/'
hist_dir = model_dir + 'histories/'
cmat_dir = model_dir + 'confusion_matrix/'
roc_dir = model_dir + 'roc/'


os.makedirs(model_dir, exist_ok=True)
os.makedirs(hist_dir, exist_ok=True)
os.makedirs(cmat_dir, exist_ok=True)
os.makedirs(roc_dir, exist_ok=True)

print('*************** Start to read data')
# Read data
inputs_train_df = pd.read_csv(dataset_dir + 'train_dataset.csv')
inputs_train = (inputs_train_df.iloc[:,:-1]).values.reshape(-1,img_size,img_size,channels,1)
labels_train = (inputs_train_df.iloc[:,-1]).values.reshape(-1,1)

inputs_test_df = pd.read_csv(dataset_dir + 'test_dataset.csv')
inputs_test = (inputs_test_df.iloc[:,:-1]).values.reshape(-1,img_size,img_size,channels,1)
labels_test = (inputs_test_df.iloc[:,-1]).values.reshape(-1,1)
print('*************** Data read')


# Create model 
model = getattr(modelsCNN, model_name)([img_size,img_size,channels,1])
model.summary()

# Train the model
weights = class_weight.compute_class_weight('balanced', classes=np.unique(labels_train.reshape(-1)), y=labels_train.reshape(-1))

history = model.fit(inputs_train, labels_train, batch_size=batch_size, epochs=epochs, class_weight=dict(enumerate(weights)), validation_split=0.1, callbacks=[K.callbacks.EarlyStopping(monitor='val_loss', patience=8, verbose=1, restore_best_weights=True)], shuffle=True, verbose=1)
          
# Save training information on accuracy and loss function
history_save = pd.DataFrame(history.history).to_hdf(hist_dir + model_name + '_history.h5', "history", append=False)

# Save model and weights
model.save(model_dir + model_name + '.h5')
print('Trained model saved @ %s ' % model_dir)

# Now evaluate the model performance
results = model.predict(inputs_test)

# Define metrics calculate values
precision = K.metrics.Precision()
recall = K.metrics.Recall()
precision.update_state(labels_test, results)
recall.update_state(labels_test, results)

precision_value = precision.result().numpy()
recall_value = recall.result().numpy()
f1_value = (2 * precision_value * recall_value)/(precision_value + recall_value)
balanced_accuracy = balanced_accuracy_score(labels_test, np.rint(results))

# Compute roc_curve and save rates
fpr, tpr, _ = roc_curve(labels_test, results)
np.savetxt(roc_dir + 'fpr.txt', fpr)
np.savetxt(roc_dir +'tpr.txt', tpr)

# Compute confusion matrix and save
cnf_matrix = confusion_matrix(labels_test, np.rint(results), normalize='true')
np.savetxt(cmat_dir + 'confusion_matrix.txt', cnf_matrix)

# Save the scores of the models
f = open(model_dir + 'total_scores.txt',"w+")
f.write("Precision: %s\n" % precision_value)
f.write("Recall: %s\n" % recall_value)
f.write("F1: %s\n" % f1_value)
f.write("BalancedAccuracy: %s" % balanced_accuracy)
f.close()
