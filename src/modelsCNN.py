import tensorflow as tf
from tensorflow import keras as K


def model1_CNN(input_shape):
    nb_filter = 4
    kernel_size = (3,3,5)
    L2_reg = K.regularizers.l2(1e-4)
    lossfunc='binary_crossentropy'

    input_img = K.Input(shape=input_shape, name = 'input_layer')

    conv1 = K.layers.Conv3D(nb_filter, kernel_size, activation='relu', padding='same', kernel_initializer='he_normal', kernel_regularizer=L2_reg, name='conv1')(input_img)
    conv2 = K.layers.Conv3D(nb_filter, kernel_size, activation='relu', padding='same', kernel_initializer='he_normal', kernel_regularizer=L2_reg, name='conv2')(conv1)
    conv3 = K.layers.Conv3D(nb_filter, kernel_size, activation='relu', padding='same', kernel_initializer='he_normal', kernel_regularizer=L2_reg, name='conv3')(conv2)

    flat = K.layers.Flatten()(conv3)
    dense1 = K.layers.Dense(512, activation='sigmoid', kernel_initializer='he_normal', kernel_regularizer=L2_reg, name='dense1')(flat)
    dense2 = K.layers.Dense(128, activation='sigmoid', kernel_initializer='he_normal', kernel_regularizer=L2_reg, name='dense2')(dense1)
    output = K.layers.Dense(1, activation='sigmoid', kernel_initializer='he_normal', kernel_regularizer=L2_reg, name='output_layer')(dense2)

    model = K.Model(inputs=input_img, outputs=output, name='model1_CNN')
    model.compile(loss=lossfunc, optimizer=K.optimizers.Adam(learning_rate=1e-4))

    return model


def model2_CNN(input_shape):
    nb_filter = 4
    kernel_size = (2,2,5)
    L2_reg = K.regularizers.l2(1e-4)
    lossfunc='binary_crossentropy'

    input_img = K.Input(shape=input_shape, name = 'input_layer')

    conv1 = K.layers.Conv3D(nb_filter, kernel_size, activation='relu', padding='valid', kernel_initializer='he_normal', kernel_regularizer=L2_reg, name='conv1')(input_img)
    conv2 = K.layers.Conv3D(nb_filter, kernel_size, activation='relu', padding='valid', kernel_initializer='he_normal', kernel_regularizer=L2_reg, name='conv2')(conv1)

    flat = K.layers.Flatten()(conv2)
    dense1 = K.layers.Dense(64, activation='sigmoid', kernel_initializer='he_normal', kernel_regularizer=L2_reg, name='dense1')(flat)
    dense2 = K.layers.Dense(8, activation='sigmoid', kernel_initializer='he_normal', kernel_regularizer=L2_reg, name='dense2')(dense1)
    output = K.layers.Dense(1, activation='sigmoid', kernel_initializer='he_normal', kernel_regularizer=L2_reg, name='output_layer')(dense2)

    model = K.Model(inputs=input_img, outputs=output, name='model2_CNN')
    model.compile(loss=lossfunc, optimizer=K.optimizers.Adam(learning_rate=1e-4))

    return model


def model3_CNN(input_shape):
    nb_filter = [4, 16]
    kernel_size = (3,3,5)
    L2_reg = K.regularizers.l2(1e-4)
    lossfunc='binary_crossentropy'

    input_img = K.Input(shape=input_shape, name = 'input_layer')

    conv1 = K.layers.Conv3D(nb_filter[0], kernel_size, activation='relu', padding='same', kernel_initializer='he_normal', kernel_regularizer=L2_reg, name='conv1')(input_img)
    conv2 = K.layers.Conv3D(nb_filter[1], kernel_size, activation='relu', padding='same', kernel_initializer='he_normal', kernel_regularizer=L2_reg, name='conv2')(conv1)

    flat = K.layers.Flatten()(conv2)
    dense1 = K.layers.Dense(2048, activation='sigmoid', kernel_initializer='he_normal', kernel_regularizer=L2_reg, name='dense1')(flat)
    dense2 = K.layers.Dense(512, activation='sigmoid', kernel_initializer='he_normal', kernel_regularizer=L2_reg, name='dense2')(dense1)
    dense3 = K.layers.Dense(64, activation='sigmoid', kernel_initializer='he_normal', kernel_regularizer=L2_reg, name='dense3')(dense2)
    output = K.layers.Dense(1, activation='sigmoid', kernel_initializer='he_normal', kernel_regularizer=L2_reg, name='output_layer')(dense3)

    model = K.Model(inputs=input_img, outputs=output, name='model3_CNN')
    model.compile(loss=lossfunc, optimizer=K.optimizers.Adam(learning_rate=1e-4))

    return model


def model4_CNN(input_shape):
    nb_filter = [4, 16, 32]
    kernel_size = (3,3,5)
    pool_size = (1,1,2)
    L2_reg = K.regularizers.l2(1e-4)
    lossfunc='binary_crossentropy'

    input_img = K.Input(shape=input_shape, name = 'input_layer')

    conv1 = K.layers.Conv3D(nb_filter[0], kernel_size, activation='relu', padding='same', kernel_initializer='he_normal', kernel_regularizer=L2_reg, name='conv1')(input_img)
    pool1 = K.layers.MaxPooling3D(pool_size, name='pool1')(conv1)

    conv2 = K.layers.Conv3D(nb_filter[1], kernel_size, activation='relu', padding='same', kernel_initializer='he_normal', kernel_regularizer=L2_reg, name='conv2')(pool1)
    pool2 = K.layers.MaxPooling3D(pool_size, name='pool2')(conv2)

    conv3 = K.layers.Conv3D(nb_filter[2], kernel_size, activation='relu', padding='same', kernel_initializer='he_normal', kernel_regularizer=L2_reg, name='conv3')(pool2)
    pool3 = K.layers.MaxPooling3D(pool_size, name='pool3')(conv3)

    flat = K.layers.Flatten()(pool3)
    dense1 = K.layers.Dense(512, activation='sigmoid', kernel_initializer='he_normal', kernel_regularizer=L2_reg, name='dense1')(flat)
    dense2 = K.layers.Dense(128, activation='sigmoid', kernel_initializer='he_normal', kernel_regularizer=L2_reg, name='dense2')(dense1)
    output = K.layers.Dense(1, activation='sigmoid', kernel_initializer='he_normal', kernel_regularizer=L2_reg, name='output_layer')(dense2)

    model = K.Model(inputs=input_img, outputs=output, name='model4_CNN')
    model.compile(loss=lossfunc, optimizer=K.optimizers.Adam(learning_rate=1e-4))

    return model


def model5_CNN(input_shape):
    nb_filter = [4, 16, 32]
    kernel_size = (3,3,5)
    pool_size = (1,1,2)
    L2_reg = K.regularizers.l2(1e-4)
    lossfunc='binary_crossentropy'

    input_img = K.Input(shape=input_shape, name = 'input_layer')

    conv1 = K.layers.Conv3D(nb_filter[0], kernel_size, activation='relu', padding='same', kernel_initializer='he_normal', kernel_regularizer=L2_reg, name='conv1')(input_img)
    #pool1 = K.layers.MaxPooling3D(pool_size, name='pool1')(conv1)

    #conv2 = K.layers.Conv3D(nb_filter[1], kernel_size, activation='relu', padding='same', kernel_initializer='he_normal', kernel_regularizer=L2_reg, name='conv2')(pool1)
    #pool2 = K.layers.MaxPooling3D(pool_size, name='pool2')(conv2)

    #conv3 = K.layers.Conv3D(nb_filter[2], kernel_size, activation='relu', padding='same', kernel_initializer='he_normal', kernel_regularizer=L2_reg, name='conv3')(pool2)
    #pool3 = K.layers.MaxPooling3D(pool_size, name='pool3')(conv3)

    flat = K.layers.Flatten()(conv1)
    #dense1 = K.layers.Dense(512, activation='sigmoid', kernel_initializer='he_normal', kernel_regularizer=L2_reg, name='dense1')(flat)
    #dense2 = K.layers.Dense(32, activation='sigmoid', kernel_initializer='he_normal', kernel_regularizer=L2_reg, name='dense2')(flat)
    output = K.layers.Dense(1, activation='sigmoid', kernel_initializer='he_normal', kernel_regularizer=L2_reg, name='output_layer')(flat)

    model = K.Model(inputs=input_img, outputs=output, name='model5_CNN')
    model.compile(loss=lossfunc, optimizer=K.optimizers.Adam(learning_rate=1e-4))

    return model